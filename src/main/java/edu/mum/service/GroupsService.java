package edu.mum.service;

import java.util.List;

import edu.mum.domain.Groups;

public interface GroupsService {
	public List<Groups> findAll();
	public Groups findOne(Long id);
}
