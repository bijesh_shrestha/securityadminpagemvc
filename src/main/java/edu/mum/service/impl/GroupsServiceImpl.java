package edu.mum.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.mum.domain.Groups;
import edu.mum.rest.service.GroupsRestService;
import edu.mum.service.GroupsService;
@Service
public class GroupsServiceImpl implements GroupsService {
	@Autowired
	private GroupsRestService groupsRestService;

	@Override
	public List<Groups> findAll() {
		// TODO Auto-generated method stub
		return groupsRestService.findAll();
	}

	@Override
	public Groups findOne(Long id) {
		// TODO Auto-generated method stub
		return  groupsRestService.findOne(id);
	}

}
