package edu.mum.rest.service;

import java.util.List;

import org.springframework.stereotype.Component;

import edu.mum.domain.Groups;


@Component
public interface GroupsRestService {
	public List<Groups> findAll();

	public Groups findOne(Long index);

	public Groups save(Groups product);
}
