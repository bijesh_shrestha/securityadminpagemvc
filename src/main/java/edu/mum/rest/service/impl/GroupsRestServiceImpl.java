package edu.mum.rest.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import edu.mum.domain.Groups;
import edu.mum.rest.RestHttpHeader;
import edu.mum.rest.service.GroupsRestService;
@Component("MVC")
public class GroupsRestServiceImpl implements GroupsRestService {
	@Autowired
	RestHttpHeader restHelper;
	
	String baseUrl = "http://localhost:8080/MemberRest/groups";
	String baseUrlExtended = baseUrl + "/";
	@Override
	public List<Groups> findAll() {
		RestTemplate restTemplate = restHelper.getRestTemplate();
		HttpEntity httpEntity = new HttpEntity(restHelper.getHttpHeaders());
		ResponseEntity<Groups[]> responseEntity = restTemplate.exchange(baseUrl, HttpMethod.GET, httpEntity, Groups[].class);	
 		List<Groups> userList = Arrays.asList(responseEntity.getBody());
		return userList;
	}
	@Override
	public Groups findOne(Long index) {
		RestTemplate restTemplate = restHelper.getRestTemplate();
		HttpEntity httpEntity = new HttpEntity(restHelper.getHttpHeaders());
		ResponseEntity<Groups> responseEntity = restTemplate.exchange(baseUrlExtended + index, HttpMethod.GET, httpEntity, Groups.class);	
		Groups member = responseEntity.getBody();
 		return member;
	}
	@Override
	public Groups save(Groups product) {
		// TODO Auto-generated method stub
		return null;
	}

}
