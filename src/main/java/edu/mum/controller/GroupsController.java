package edu.mum.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.mum.domain.Groups;
import edu.mum.service.GroupsService;


@Controller
@RequestMapping("/groups")
public class GroupsController {
	@Autowired
	private GroupsService groupsService;
	@RequestMapping("")
	public String listMembers(Model model) {
		List<Groups> groups = groupsService.findAll();
		model.addAttribute("groups", groups);
		return "groups";
	}
}
